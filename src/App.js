import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Button, Card } from "react-bootstrap";

const gitHub_user = "https://api.github.com/users/tiree24";

class App extends React.Component {
  state = {
    user: {},
    active: false,
  };

  handleToggle = () => {
    if (this.state.active === true) {
      this.setState({ active: false });
    } else {
      this.setState({ active: true });
    }
    fetch(gitHub_user)
      .then((response) => response.json())
      .then((data) => {
        this.setState({ user: data });
      });
  };

  render() {
    return (
      <React.Fragment>
        <div>
          <Button onClick={this.handleToggle}>Click</Button>
        </div>
        {this.state.active === true && (
          <Card>
            <img src={this.state.user.avatar_url} />
            <Card.Header>{this.state.user.login}</Card.Header>
            <Card description={this.state.user.bio} />
          </Card>
        )}
      </React.Fragment>
    );
  }
}

export default App;
